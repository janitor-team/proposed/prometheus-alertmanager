Source: prometheus-alertmanager
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Martina Ferrari <tina@debian.org>,
           Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>,
Section: net
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang (>= 1.31~),
               dh-sequence-bash-completion,
               golang-any (>= 2:1.10~),
               golang-github-alecthomas-units-dev,
               golang-github-cenkalti-backoff-dev,
               golang-github-cespare-xxhash-dev,
               golang-github-go-kit-kit-dev,
               golang-github-go-openapi-errors-dev (>= 0.15.0~),
               golang-github-go-openapi-loads-dev,
               golang-github-go-openapi-runtime-dev (>= 0.15.0~),
               golang-github-go-openapi-spec-dev,
               golang-github-go-openapi-strfmt-dev,
               golang-github-go-openapi-swag-dev,
               golang-github-go-openapi-validate-dev,
               golang-github-gogo-protobuf-dev (>= 0.5),
               golang-github-hashicorp-go-sockaddr-dev,
               golang-github-hashicorp-memberlist-dev (>= 0.1.0~),
               golang-github-kylelemons-godebug-dev,
               golang-github-oklog-run-dev,
               golang-github-oklog-ulid-dev,
               golang-github-pkg-errors-dev,
               golang-github-prometheus-client-golang-dev (>= 1.3.0~),
               golang-github-prometheus-common-dev (>= 0.7.0~),
               golang-github-rs-cors-dev,
               golang-github-satori-go.uuid-dev,
               golang-github-stretchr-testify-dev,
               golang-github-xlab-treeprint-dev (>= 0.0~git20180615),
               golang-go-flags-dev,
               golang-golang-x-net-dev,
               golang-gopkg-alecthomas-kingpin.v2-dev,
               golang-gopkg-yaml.v2-dev (>= 2.2.1-1~),
               golang-protobuf-extensions-dev,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-alertmanager
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-alertmanager.git
Homepage: https://prometheus.io/
XS-Go-Import-Path: github.com/prometheus/alertmanager

Package: prometheus-alertmanager
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: adduser,
         ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: handle and deliver alerts created by Prometheus
 The Alertmanager handles alerts sent by client applications such as the
 Prometheus server. It takes care of deduplicating, grouping, and routing them
 to the correct receiver integration such as email, PagerDuty, or OpsGenie. It
 also takes care of silencing and inhibition of alerts.

Package: golang-github-prometheus-alertmanager-dev
Section: golang
Architecture: all
Multi-Arch: foreign
Depends: golang-github-alecthomas-units-dev,
         golang-github-cenkalti-backoff-dev,
         golang-github-cespare-xxhash-dev,
         golang-github-go-kit-kit-dev,
         golang-github-go-openapi-errors-dev (>= 0.15.0~),
         golang-github-go-openapi-loads-dev,
         golang-github-go-openapi-runtime-dev (>= 0.15.0~),
         golang-github-go-openapi-spec-dev,
         golang-github-go-openapi-strfmt-dev,
         golang-github-go-openapi-swag-dev,
         golang-github-go-openapi-validate-dev,
         golang-github-gogo-protobuf-dev (>= 0.5),
         golang-github-hashicorp-go-sockaddr-dev,
         golang-github-hashicorp-memberlist-dev (>= 0.1.0~),
         golang-github-kylelemons-godebug-dev,
         golang-github-oklog-run-dev,
         golang-github-oklog-ulid-dev,
         golang-github-pkg-errors-dev,
         golang-github-prometheus-client-golang-dev (>= 1.3.0~),
         golang-github-prometheus-common-dev (>= 0.7.0~),
         golang-github-rs-cors-dev,
         golang-github-satori-go.uuid-dev,
         golang-github-stretchr-testify-dev,
         golang-github-xlab-treeprint-dev (>= 0.0~git20180615),
         golang-go-flags-dev,
         golang-golang-x-net-dev,
         golang-gopkg-alecthomas-kingpin.v2-dev,
         golang-gopkg-yaml.v2-dev (>= 2.2.1-1~),
         golang-protobuf-extensions-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Description: handle and deliver alerts created by Prometheus -- source
 The Alertmanager handles alerts sent by client applications such as the
 Prometheus server. It takes care of deduplicating, grouping, and routing them
 to the correct receiver integration such as email, PagerDuty, or OpsGenie. It
 also takes care of silencing and inhibition of alerts.
 .
 This package provides the source code to be used as a library.
